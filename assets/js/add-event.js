$("#events_form_save").click(function(e) {
    e.preventDefault();
    $.post('/save-event-data', {
        event: $("#events_form_event").val(),
        date: $("#events_form_date").val(), 
        club: $("#events_form_club").val(),
        facebook: $("#events_form_facebook").val(), 
        image: $("#image").val()
    }, function(){ 
        $(".c-detail-form__container, .c-detail-form__image")
            .addClass("c-detail-form__container--disabled");
        $(".c-detail-form__success")
            .addClass("c-detail-form__success--visible");
        setTimeout(function() {
            history.back();
        }, 2500); 
    });
});

$croppieSearch = $('#croppie').croppie({
    enableExif: true,
    viewport: {
        width: 310,
        height: 440
    },
    boundary: {
        width: 500,
        height: 600
    }
});

$('#croppie-btn').click(function(){
    $('#modal').addClass('c-croppie__modal--visible');
});

$('#upload').on('change', function () { 
    var reader = new FileReader();
    reader.onload = function (e) {
        $croppieSearch.croppie('bind', {
            url: e.target.result
        }).then(function(){
            console.log('ahoj');
        });
    }
    reader.readAsDataURL(this.files[0]);
});

$('#croppie-upload').on('click', function(ev) {   
    $croppieSearch.croppie('result', {
        type: 'canvas',
        size: 'viewport'
    }).then(function(response) {
        $.ajax({
            url: "/save-event-img",
            type: "POST",
            data: {
                "image": response
            },
            success: function(data) {
                html = '<img src="' + response + '" />';
                data = data.trim();
                $("#upload-preview").html(html);
                $("#image").attr('value', data); 
            }
        });
        $('.c-croppie__modal').removeClass('c-croppie__modal--visible');
        $('#upload-preview').addClass('c-croppie__preview--visible')
    });
});