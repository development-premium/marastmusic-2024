$('.c-text *:not(img)').removeAttr("style");

$('.c-text img').filter(function() {
    return $(this).width() === $(this).parent().width();
}).addClass('c-text__img--fullwidth');

console.log($('.c-text img').filter(function() {
    return $(this).width() === $(this).parent().width();
}).addClass('c-text__img--fullwidth'));
