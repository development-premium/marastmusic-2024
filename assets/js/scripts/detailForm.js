$('textarea').on('input', function () {
	this.style.height = 'auto';
	this.style.height = (this.scrollHeight) + 'px';
});

$('#detailFormSubmit').click(function(e) {
	e.preventDefault();
	var $button = $(this);
	if ($('#comments_form_name').val() != '' && $('#comments_form_text').val() != '') {
		$(this).addClass('c-detail-form__button--clicked');
		$.post($button.data('url'), {
			'comments_form[name]': $('#comments_form_name').val(),
			'comments_form[text]': $('#comments_form_text').val(),
			'comments_form[contentobject_id]': $('#comments_form_contentobject_id').val(),
			'comments_form[url]': $('#comments_form_url').val(),
			'comments_form[title]': $('#comments_form_title').val(),
			'comments_form[type]': $('#comments_form_type').val(),
			'comments_form[_token]': $('#comments_form__token').val(),
			'headerTitle': $('.c-header-large__title').text()
		}, function() {
			$('.c-detail-form__input').val('');
			$('#comments').load('/comments-detail', {contentObjectId: $('#comments_form_contentobject_id').val()});
		});
	}
});

$('.c-detail-form__input').focus(function(){
	$('#detailFormSubmit').removeClass('c-detail-form__button--clicked');
});