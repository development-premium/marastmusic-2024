$('#articlesMenuList > li').each(function(){
	if ($(this).hasClass('c-detail-menu__item--active')) {
		$(this).find('a').click();
		history.pushState({}, null, 'clanky');
	}
});

function loadArticles(button, template) {
	button.click(function(){
		$('#main').load(template + '?timestamp=' + new Date().getTime());
		$('.c-detail-menu__item').removeClass('c-detail-menu__item--active');
		$(this).addClass('c-detail-menu__item--active');
	});
}

loadArticles($('#articles #all'), 'articles-board/all');
loadArticles($('#articles #reviews'), 'articles-board/reviews');
loadArticles($('#articles #reports'), 'articles-board/reports');
loadArticles($('#articles #interviews'), 'articles-board/interviews');
loadArticles($('#articles #albums'), 'articles-board/albums');
loadArticles($('#articles #others'), 'articles-board/others');
