$('#homepage #main').load('/articles-result/1');

function loadList(button, template) {
	button.click(function(){
		$('#main').load(template + '?timestamp=' + new Date().getTime());
		$('.c-center-menu__item').removeClass('c-center-menu__item--active');
		$(this).addClass('c-center-menu__item--active');
	});
}

loadList($('#homepage #all'), '/articles-result/1');
loadList($('#homepage #reviews'), '/articles-result/2');
loadList($('#homepage #reports'), '/articles-result/3');
loadList($('#homepage #interviews'), '/articles-result/4');
loadList($('#homepage #albums'), '/articles-result/5');
loadList($('#homepage #others'), '/articles-result/6');
