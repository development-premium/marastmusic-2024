if ($('#articles').length) {

	var activeId = $('.c-detail-menu__item--active').attr('id'),
		url = overviewTemplate + '/' + activeId;

	$.ajax({
		url: url,
		type: 'POST',
		data: {
			boxCount: 0,
			activeId: activeId
		},
		success: function(response) {
			$('<div></div>').appendTo('#main').html(response);
		}
	});

	$('#btnMore').click(function() {
		$('.c-pagination__page-button').hide();

		var boxCount = $('.c-item-v4').length,
			activeId = $('.c-detail-menu__item--active').attr('id'),
			url = overviewTemplate + '/' + activeId;
		
		$.ajax({
			url: url,
			type: 'POST',
			data: {
				boxCount: boxCount,
				activeId: activeId
			},
			success: function(response) {
				$('<div></div>').appendTo('#main').html(response);
			}
		});
	});
}