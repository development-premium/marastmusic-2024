$('#slideToCommentSection').click(function() {
    $([document.documentElement, document.body]).animate(
        {
            scrollTop: $('.c-detail-comments').offset().top,
        },
        2000
    );
});

$('.deleteComment').click(function(e) {
	if(confirm('Opravdu smazat ???')) {
		$.post('/delete-comment/', {
			id: $(this).attr('data-id')
		}, function() {
			$('#comments').load('/load-comments', {contentObjectId: $('#contentObjectId').val()});
		});
	} else {
		e.preventDefault();
	}
});

function loadCommentsSection(button, template) {
	button.click(function(){
		$('#comments').addClass('c-loader');
		$('.c-item-v2').addClass('c-item-v2--invisible');
		$('#comments').load(template + '?timestamp=' + new Date().getTime(), function() {
			$('#comments').removeClass('c-loader');
			$('.c-item-v2').removeClass('c-item-v2--invisible');
		});
		$('.c-text-filter__item').removeClass('c-text-filter__item--active');
		$(this).addClass('c-text-filter__item--active');
	});
}

if ($('#homepage').length ||
	$('#overview').length || 
	$('#articles').length || 
	$('#gallery-overview').length || 
	$('#events-overview').length) {
	$('#comments').load('/comments/1.php');
}

loadCommentsSection($('#commentsAll'), '/comments/1');
loadCommentsSection($('#commentsReports'), '/comments/2');
loadCommentsSection($('#commentsReviews'), '/comments/3');
loadCommentsSection($('#commentsOthers'), '/comments/4');