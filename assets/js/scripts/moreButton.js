$('#btnMore').click(function() {
	if ($('#overview').length || $('#tags-overview').length) {
		var boxCount = $('#main .c-item-v4').length;
	}
	
	if ($('#events-overview').length) {
		var boxCount = $('#main .c-box-v2').length;
	}

	if ($('#gallery-overview').length) {
		var boxCount = $('#main .c-box-v5').length;
	}

	if (!$('#articles').length) {
		var url = overviewTemplate + '/' + boxCount;

		if (typeof tag !== 'undefined' && tag !== null) {
			var postData = { tag: tag },
				url = window.location.origin + '/' + url;

			$.ajax({
				type: "POST",
				url: url,
				data: postData,
				success: function(response) {
					$('<div></div>').html(response).appendTo('#main');
				}
			});
		} else {
			$('<div></div>').appendTo('#main').load(url);
		}
	}
});

if ($('#overview').length ||
	$('#events-overview').length ||
	$('#gallery-overview').length) {
	$('#main').load(overviewTemplate);
}

if (typeof tag !== 'undefined' && tag !== null) {
	var postData = { tag: tag };

	$.ajax({
		type: "POST",
		url: window.location.origin + '/' + overviewTemplate,
		data: postData,
		success: function(response) {
			$('#main').html(response);
		}
	});
}