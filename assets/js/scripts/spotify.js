$('#spotifyPlayer').load('/spotify-playlist/1');

function showPlaylist(button, template) {
	button.click(function(){
		$('#spotifyPlayer').addClass('c-spotify__player--loading');
		$('#spotifyPlayer').load(template, function(){
			$('#spotifyPlayer iframe').attr('id', 'iframeId');
			$('#iframeId').on('load', function(){
				$('#spotifyPlayer').removeClass('c-spotify__player--loading');
			});
		});
		$('.c-spotify__playlist').removeClass('c-spotify__playlist--active');
		$(this).addClass('c-spotify__playlist--active');
	});
}

showPlaylist($('#playlist1'), '/spotify-playlist/1');
showPlaylist($('#playlist2'), '/spotify-playlist/2');
showPlaylist($('#playlist3'), '/spotify-playlist/3');
showPlaylist($('#playlist4'), '/spotify-playlist/4');