var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('sass', function() {
	return gulp.src('assets/sass/main.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('public/css'));
});
 
gulp.task('scripts', function() {
	return gulp.src([
		'assets/js/wrap/_open.txt',
		'assets/js/scripts/articlesFullOverview.js',
		'assets/js/scripts/articlesHomepage.js',
		'assets/js/scripts/articleSlider.js',
		'assets/js/scripts/comments.js',
		'assets/js/scripts/darkmode.js',		
		'assets/js/scripts/detail.js',
		'assets/js/scripts/detailForm.js',
		'assets/js/scripts/eventsSlider.js',
		'assets/js/scripts/header.js',
		'assets/js/scripts/menu.js',
		'assets/js/scripts/submenu.js',
		'assets/js/scripts/moreButton.js',		
		'assets/js/scripts/moreButtonArticles.js',
		'assets/js/scripts/search.js',
		'assets/js/scripts/spotify.js',
		'assets/js/wrap/_close.txt'
	])
		.pipe(concat('main.js'))
		.pipe(uglify())
		.pipe(gulp.dest('public/js'));
});

gulp.task('add-event', function() {
    return gulp.src('assets/js/add-event.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

gulp.task('watch', function() {
	gulp.watch('assets/sass/**/*.scss', gulp.series('sass'));
	gulp.watch('assets/js/scripts/*.js', gulp.series('scripts'));
	gulp.watch('assets/js/add-event.js', gulp.series('add-event'));
});