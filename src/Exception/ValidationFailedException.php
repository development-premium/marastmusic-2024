<?php

namespace App\Exception;

class ValidationFailedException extends \Exception {
    protected $violations;

    public function __construct($violations, $message = "", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
        $this->violations = $violations;
    }

    public function getViolations() {
        return $this->violations;
    }
}