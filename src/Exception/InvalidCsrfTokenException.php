<?php

namespace App\Exception;

class InvalidCsrfTokenException extends \Exception {

    public function __construct($message = "Invalid CSRF token", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}
