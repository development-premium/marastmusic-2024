<?php

namespace App\Mapper;

use App\DTO\CommentsDTO;
use App\Entity\Comments;

class CommentsMapper {
    public function mapDtoToEntity(CommentsDTO $dto): Comments {
        
        $comment = new Comments();

        $comment->setName($dto->getName());
        $comment->setText($dto->getText());
        $comment->setContentObjectId($dto->getContentObjectId());
        $comment->setUrl($dto->getUrl());
        $comment->setTitle($dto->getTitle());
        $comment->setType($dto->getType());
        $comment->setUrlPath($dto->getUrlPath());
        $comment->setDetailTitle($dto->getDetailTitle());

        return $comment;
    }
}
