<?php

namespace App\Service;

use App\Repository\GalleryRepository;
use App\Repository\ImagesRepository;

class GalleryService
{    
    private $galleryRepository;
    private $imagesRepository;
    private $dataParserService;

    public function __construct(GalleryRepository $galleryRepository, ImagesRepository $imagesRepository, DataParserService $dataParserService)
    {
        $this->galleryRepository = $galleryRepository;
        $this->imagesRepository = $imagesRepository;
        $this->dataParserService = $dataParserService;
    }

    public function getGalleryData(): array
    {        
        $galeries = $this->galleryRepository->findLastTwoGalleries();
        
        foreach($galeries as $gallery) {
            $urlPath = $gallery->getUrlPath();
            $gallery->setPhotosCount($this->galleryRepository->findNumberOfPhotos($urlPath));
            $gallery->setAuthorName($this->dataParserService->parseAuthorName($gallery->getAuthorName()));
        }

        return $galeries;
    }

    public function getGalleryDetail(string $galleryName) {        
        return $this->galleryRepository->findGalleryData($galleryName);
    } 

    public function getImages(string $galleryName) {
        return $this->imagesRepository->findImages($galleryName);
    }    

    public function getBoardData(int $boxCount): array
    {
        $boardData = $this->galleryRepository->getGalleryBoard($boxCount, 4);

        foreach ($boardData as $key => &$boardItem) {       
            $boardData[$key]['author_name'] = $this->dataParserService->parseAuthorName($boardItem['author_name']);
            $boardData[$key]['number_of_photos'] = $this->galleryRepository->findNumberOfPhotos($boardItem['url_path']);
        }

        return $boardData;
    }
}