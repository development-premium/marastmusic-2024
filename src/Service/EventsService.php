<?php

namespace App\Service;

use App\Entity\UserEvents;
use App\Repository\EventsRepository;
use Symfony\Component\HttpFoundation\Request;

class EventsService
{    
    private $eventsRepository;

    public function __construct(EventsRepository $eventsRepository)
    {
        $this->eventsRepository = $eventsRepository;
    }

    public function saveUserEvent(Request $request): UserEvents
    {
        $event = new UserEvents();

        $event->setEvent($request->request->get('event'));
        $event->setDate($request->request->get('date'));
        $event->setClub($request->request->get('club'));
        $event->setFacebook($request->request->get('facebook'));
        $event->setImage($request->request->get('image'));
        $event->setCreated(time());     

        $this->eventsRepository->save($event);

        $emailContent = "Návštěvník marastu přidal novou akci: " . $request->request->get('event');

        mail('bizzaro@marastmusic.com', 'NOVÁ AKCE NA MARASTU', $emailContent);
		mail('loomis@marastmusic.com', 'NOVÁ AKCE NA MARASTU', $emailContent);
		mail('hubicka.jakub@seznam.cz', 'NOVÁ AKCE NA MARASTU', $emailContent);      
	
        return $event;
    }

    public function getEventsData(): array
    {
        return $this->eventsRepository->findEvents();
    }

    public function getDetail(string $url, string $type): array
    {
        return $this->eventsRepository->findEventData($url, $type);
    }

    public function getBoardData(int $boxCount): array
    {
        return $this->eventsRepository->getEventsBoard($boxCount);
    }
}