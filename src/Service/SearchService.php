<?php 

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class SearchService
{
    private $entityManager;
    private $dataParserService;

    public function __construct(EntityManagerInterface $entityManager, DataParserService $dataParserService)
    {
        $this->entityManager = $entityManager;
        $this->dataParserService = $dataParserService;
    }

    public function getList(string $searchString, string $searchType): array
    {
        $connection = $this->entityManager->getConnection();

        switch ($searchType) {
            case 'search_news':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference
                        FROM news
                        WHERE title LIKE ?";
                break;

            case 'search_interviews':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference
                        FROM interviews
                        WHERE title LIKE ?";
                break;

            case 'search_reviews':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference
                        FROM reviews
                        WHERE title LIKE ?";
                break;

            case 'search_reports':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference
                        FROM reports
                        WHERE title LIKE ?";
                break;

            case 'search_gallery':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference
                        FROM gallery
                        WHERE title LIKE ?";
                break;

            case 'search_albums':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference 
                        FROM articles 
                        WHERE category = 'Alba měsíce' AND title LIKE ?";
                break;

            case 'search_others':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference 
                        FROM articles 
                        WHERE NOT category = 'Alba měsíce' AND title LIKE ?";
                break;

            case 'search_all':
                $querySearchString = "%" . $searchString . "%";
                $sql = "SELECT url_path, author_name, type, publish_date, title, image_reference FROM articles WHERE title LIKE ?
                        UNION ALL
                        SELECT url_path, author_name, type, publish_date, title, image_reference FROM interviews WHERE title LIKE ?
                        UNION ALL
                        SELECT url_path, author_name, type, publish_date, title, image_reference FROM reports WHERE title LIKE ?
                        UNION ALL
                        SELECT url_path, author_name, type, publish_date, title, image_reference FROM gallery WHERE title LIKE ?
                        UNION ALL
                        SELECT url_path, author_name, type, publish_date, title, image_reference FROM news WHERE title LIKE ?
                        ORDER BY publish_date DESC";
                break;
        }

        $statement = $connection->prepare($sql);
        $statement->bindValue(1, $querySearchString);
        $statement->bindValue(2, $querySearchString);
        $statement->bindValue(3, $querySearchString);
        $statement->bindValue(4, $querySearchString);
        $statement->bindValue(5, $querySearchString);
        $result = $statement->executeQuery();

        $resultArray = $result->fetchAllAssociative();

        foreach ($resultArray as $key => &$item) {       
            $resultArray[$key]['author_name'] = $this->dataParserService->parseAuthorName($item['author_name']);
        }

        return $resultArray;
    }
}
