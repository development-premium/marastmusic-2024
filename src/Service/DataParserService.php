<?php

namespace App\Service;

class DataParserService
{
    public function parseAuthorName(string $authorString): string
    {
        if (preg_match('/<name>(.*?)<\/name>/', $authorString, $matches)) {
            return $matches[1];
        } else {
            return $authorString;
        }
    }
}
