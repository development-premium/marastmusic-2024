<?php

namespace App\Service;

class SpotifyService
{
    const PLAYLIST_1 = 'home/spotify/playlists/playlist-1.html.twig';
    const PLAYLIST_2 = 'home/spotify/playlists/playlist-2.html.twig';
    const PLAYLIST_3 = 'home/spotify/playlists/playlist-3.html.twig';
    const PLAYLIST_4 = 'home/spotify/playlists/playlist-4.html.twig';

    public function getPlaylist(string $id): string
    {
        switch ($id) {
            case '1':
                return self::PLAYLIST_1;
            case '2':
                return self::PLAYLIST_2;
            case '3':
                return self::PLAYLIST_3;
            case '4':
                return self::PLAYLIST_4;
            default:
                return self::PLAYLIST_1;
        }
    }
}