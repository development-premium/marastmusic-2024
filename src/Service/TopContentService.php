<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class TopContentService
{
    private $entityManager;
    private $dataParserService;

    public function __construct(EntityManagerInterface $entityManager, DataParserService $dataParserService)
    {
        $this->entityManager = $entityManager;
        $this->dataParserService = $dataParserService;
    }

    public function getContent(): array 
    {
        $list = $this->getList();

        foreach ($list as &$item) {
            $item['author_name'] = $this->dataParserService->parseAuthorName($item['author_name']);
        }

        return array_slice($list, 1);
    }

    public function getTopItem(): array
    {
        return $this->getList()[0];
    }

    private function getList(): array
    {
        $connection = $this->entityManager->getConnection();

        $sql = "SELECT id, views, comments_count, url_path, author_name, type, publish_date, title, image_reference FROM articles
                UNION ALL
                SELECT id, views, comments_count, url_path, author_name, type, publish_date, title, image_reference FROM interviews
                UNION ALL
                SELECT id, views, comments_count, url_path, author_name, type, publish_date, title, image_reference FROM reports
                UNION ALL
                SELECT id, views, comments_count, url_path, author_name, type, publish_date, title, image_reference FROM reviews
                UNION ALL
                SELECT id, views, comments_count, url_path, author_name, type, publish_date, title, image_reference FROM news
                ORDER BY views DESC LIMIT 4";
        
        $statement = $connection->prepare($sql);
        $result = $statement->executeQuery();

        return $result->fetchAllAssociative();
    }
}
