<?php

namespace App\Service;

use Doctrine\DBAL\Connection;

class TagsService
{
    private $connection;
    private $dataParserService;

    public function __construct(Connection $connection, DataParserService $dataParserService)
    {
        $this->connection = $connection;
        $this->dataParserService = $dataParserService;
    }

    public function getBoardList(int $boxCount, string $tag): array
    {
        $tag = "%" . $tag . "%";

        $sql = "SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM articles WHERE tags LIKE :tag
                UNION ALL
                SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM interviews WHERE tags LIKE :tag
                UNION ALL
                SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM reports WHERE tags LIKE :tag
                UNION ALL
                SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM gallery WHERE tags LIKE :tag
                UNION ALL
                SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM reviews WHERE tags LIKE :tag
                UNION ALL
                SELECT id, url_path, author_name, type, publish_date, title, image_reference, comments_count FROM news WHERE tags LIKE :tag
                ORDER BY publish_date DESC LIMIT :boxCount, 9";

        $stmt = $this->connection->prepare($sql);

        $stmt->bindValue('tag', $tag);
        $stmt->bindValue('boxCount', $boxCount, \PDO::PARAM_INT);

        $result = $stmt->executeQuery();

        $resultArray = $result->fetchAllAssociative();

        foreach ($resultArray as &$item) {
            $item['author_name'] = $this->dataParserService->parseAuthorName($item['author_name']);
        }

        return $resultArray;
    }
}