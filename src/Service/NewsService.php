<?php

namespace App\Service;

use App\Entity\News;
use App\Repository\NewsRepository;

class NewsService
{    
    private $newsRepository;
    private $detailService;
    private $dataParserService;

    public function __construct(
        NewsRepository $newsRepository,
        DetailService $detailService,
        DataParserService $dataParserService,
    ) {
        $this->newsRepository = $newsRepository;
        $this->detailService = $detailService;
        $this->dataParserService = $dataParserService;
    }

    public function getNewsData(): array
    {
        $newsItems = $this->newsRepository->findLastNews();

        foreach ($newsItems as $item) {
            if (strpos($item->getUrlPath(), 'novinky/') === 0) {
                $newUrlPath = substr($item->getUrlPath(), 8);
                $item->setUrlPath($newUrlPath);
            }
            $item->setAuthorName($this->dataParserService->parseAuthorName($item->getAuthorName()));
        }

        return $newsItems;
    }

    public function getDetail(string $url): array
    {
        $url = 'novinky/' . $url;
        $detailData = $this->newsRepository->findDetailData($url);
        return $this->detailService->parseDetailData($detailData);
    }
 
    public function getBoardData(int $boxCount): array
    {
        $boardData = $this->newsRepository->findBoardData($boxCount);
            
        foreach ($boardData as $key => &$boardItem) {       
            $boardData[$key]['author_name'] = $this->dataParserService->parseAuthorName($boardItem['author_name']);
        }

        return $boardData;
    }
}