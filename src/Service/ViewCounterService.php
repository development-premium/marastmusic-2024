<?php 


namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;

class ViewCounterService
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function incrementView(string $type, string $urlPath): void
    {
        switch ($type) {
            case 'report':
                $urlPath = 'clanky/' . $urlPath;
                $this->updateViews('App\Entity\Reports', $urlPath);
                break;

            case 'recenze':
                $urlPath = 'recenze/' . $urlPath;
                $this->updateViews('App\Entity\Reviews', $urlPath);
                break;

            case 'novinka':
                $urlPath = 'novinky/' . $urlPath;
                $this->updateViews('App\Entity\News', $urlPath);
                break;

            case 'interview':
                $urlPath = 'clanky/' . $urlPath;
                $this->updateViews('App\Entity\Interviews', $urlPath);
                break;

            case 'article':
                $urlPath = 'clanky/' . $urlPath;
                $this->updateViews('App\Entity\Articles', $urlPath);
                break;
        }
    }

    private function updateViews(string $entityClass, string $urlpath): void
    {
        $repository = $this->entityManager->getRepository($entityClass);
        $entity = $repository->findOneBy(['url_path' => $urlpath]);

        if ($entity) {
            $entity->incrementViews();
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
    }
}