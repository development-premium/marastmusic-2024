<?php

namespace App\Service;

use App\Entity\Box;
use App\Repository\BoxRepository;

class BoxService
{    
    private $boxRepository;

    public function __construct(BoxRepository $boxRepository)
    {
        $this->boxRepository = $boxRepository;
    }

    public function getBoxData(): ?Box
    {
        return $this->boxRepository->findLastRecord();
    }
}