<?php

namespace App\Service;

use App\DTO\CommentsDTO;
use App\Exception\InvalidCsrfTokenException;
use App\Exception\ValidationFailedException;
use App\Mapper\CommentsMapper;
use App\Repository\ArticlesRepository;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\CommentsRepository;
use App\Repository\InterviewsRepository;
use App\Repository\NewsRepository;
use App\Repository\ReportsRepository;
use App\Repository\ReviewsRepository;

class CommentsService
{    
    private const REPORTS = 'report';
    private const REVIEWS = 'Review';
    private const OTHERS = 'others';
    
    private $commentsRepository;
    private $csrfTokenManager;
    private $validator;
    private $commentsMapper;
    private $articlesRepository;
    private $newsRepository;
    private $reportsRepository;
    private $reviewsRepository;
    private $interviewsRepository;
    
    public function __construct(
        CommentsRepository $commentsRepository, 
        CsrfTokenManagerInterface $csrfTokenManager,
        ValidatorInterface $validator,
        CommentsMapper $commentsMapper,
        ArticlesRepository $articlesRepository,
        NewsRepository $newsRepository,
        ReportsRepository $reportsRepository,
        ReviewsRepository $reviewsRepository,
        InterviewsRepository $interviewsRepository,
    ) {
        $this->commentsRepository = $commentsRepository;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->validator = $validator;
        $this->commentsMapper = $commentsMapper;
        $this->articlesRepository = $articlesRepository;
        $this->newsRepository = $newsRepository;
        $this->reportsRepository = $reportsRepository;
        $this->reviewsRepository = $reviewsRepository;
        $this->interviewsRepository = $interviewsRepository;
    }

    public function getResult(string $id): array
    {
        switch ($id) {
            case '1':
                return $this->commentsRepository->findAllComments();
                break;
            case '2':
                return $this->commentsRepository->findCommentsBasedOnType(self::REPORTS);
                break;
            case '3':
                return $this->commentsRepository->findCommentsBasedOnType(self::REVIEWS);
                break;
            case '4':
                return $this->commentsRepository->findCommentsBasedOnType(self::OTHERS);
                break;
            
            default:
                return $this->commentsRepository->findAllComments();
                break;
        }
    }

    public function handleCommentsData(array $formData): void
    {
        $data = $formData['comments_form'];
     
        $csrfTokenValue = $data['_token'];
        $csrfToken = new CsrfToken('comments_form', $csrfTokenValue);

        if (!$this->csrfTokenManager->isTokenValid($csrfToken)) {
            throw new InvalidCsrfTokenException('Invalid CSRF token');
        }

        $commentsDTO = new CommentsDTO();
        $commentsDTO->setName($data['name'] ?? null);
        $commentsDTO->setText($data['text'] ?? null);
        $commentsDTO->setContentObjectId($data['contentobject_id'] ?? null);
        $commentsDTO->setUrl($data['url'] ?? null);
        $commentsDTO->setTitle($data['title'] ?? null);
        $commentsDTO->setType($data['type'] ?? null);
        $commentsDTO->setDetailTitle($formData['headerTitle'] ?? null);
        $commentsDTO->setUrlPath($formData['urlPath'] ?? null);
           
        $violations = $this->validator->validate($commentsDTO);

        if (count($violations) > 0) {
            throw new ValidationFailedException($violations, 'Data is invalid');
        }

        if (!empty($commentsDTO->getName()) && !empty($commentsDTO->getText())) {            
            $comment = $this->commentsMapper->mapDtoToEntity($commentsDTO);
            $this->commentsRepository->saveComment($comment);
            $this->increaseCommentsCount($data['type'], $data['url']);
        }
    }

    public function createDefaultCommentsDTO(array $detail, string $url): CommentsDTO
    {
        $commentsDTO = new CommentsDTO();
        
        $commentsDTO->contentObjectId = $detail['contentobject_id'] ?? null;
        $commentsDTO->url = $url;
        $commentsDTO->title = $detail['title'] ?? null;
        $commentsDTO->type = $detail['type'] ?? null;

        return $commentsDTO;
    }

    public function getDetail(int $contentObjectId): array
    {
        $comments = $this->commentsRepository->getDetailComments($contentObjectId);

        foreach ($comments as &$comment) {
            $comment['created'] = date('d.m.y H:i:s', $comment['created']);

            if ($comment['is_old'] === 1 && \strpos($comment['text'], '[quote')) {
				$quote = $this->getQuote($comment['text'], '[quote', '[/quote]');
				$quoteWrapped = '<span class="c-item-v2__quote">"' . $quote . '"</span> ';
				$comment['text'] = preg_replace('#\[quote[^\]]*\](.*?)\[/quote\]#m', "$1", str_replace($quote, $quoteWrapped, $comment['text']));
			}
        }

        return $comments;
    }

    private function getQuote(string $comment, string $startTag, string $endTag): string {

		$comment = ' ' . $comment;
		$startTagPosition = strpos($comment, $startTag);

		if ($startTagPosition === 0) {
			return '';
		}

		$commentPosition = $startTagPosition + strlen($startTag) + 7;   
		$quoteLength = strpos($comment, $endTag, $commentPosition) - $commentPosition;

		return substr($comment, $commentPosition, $quoteLength);
	}

    private function increaseCommentsCount(string $type, string $url): void
    {
        switch ($type) {
            case 'novinka':
                $this->newsRepository->incrementCommentsCountForNews($url);
                break;

            case 'report':
                $this->reportsRepository->incrementCommentsCountForReports($url);
                break;

            case 'rozhovor':
                $this->interviewsRepository->incrementCommentsCountForInterviews($url);
                break;

            case 'recenze':
                $this->reviewsRepository->incrementCommentsCountForReviews($url);
                break;

            case 'article':
                $this->articlesRepository->incrementCommentsCountForArticles($url);
                break;
        }
    }
}
