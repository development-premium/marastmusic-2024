<?php

namespace App\Service;

use App\Repository\RelatedRepository;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DetailService
{    
    private $urlGenerator;
    private $relatedRepository;
    private $dataParserService;

    public function __construct(
        UrlGeneratorInterface $urlGenerator, 
        RelatedRepository $relatedRepository, 
        DataParserService $dataParserService
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->relatedRepository = $relatedRepository;
        $this->dataParserService = $dataParserService;
    }
    
    public function parseDetailData(array $data): array
    {
        if (isset($data['tags'])) {
            $tagsArray = explode(",", $data['tags']);
            $tags = [];

            if (!empty($tagsArray)) {
                foreach ($tagsArray as $tag) {
                    $tag = trim($tag);
                    $tagLink = str_replace(' ', '_', $tag);
                    $tags[] = [
                        'name' => $tag,
                        'link' => $this->urlGenerator->generate('app_tags', ['tag' => $tagLink], UrlGeneratorInterface::ABSOLUTE_URL)
                    ];
                }
            }

            $data['tags'] = $tags;
        }

        if (isset($data['author_name'])) {
            $data['author_name'] = $this->dataParserService->parseAuthorName($data['author_name']);
        }
        
        return $data;
    }

    public function getRelatedContent(string $relatedContent): array
    {
        $relatedContentData = $this->relatedRepository->getRelated($relatedContent);

        foreach($relatedContentData as $item) {
            $item->setAuthorName($this->dataParserService->parseAuthorName($item->getAuthorName()));        
        }

        return $relatedContentData;
    }
}