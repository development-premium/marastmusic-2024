<?php

namespace App\Service;

use App\Repository\IntroRepository;

class IntroService
{    
    private $introRepository;

    public function __construct(IntroRepository $introRepository)
    {
        $this->introRepository = $introRepository;
    }

    public function getIntroData(): array
    {
        return $this->introRepository->findAll();
    }
}