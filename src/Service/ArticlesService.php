<?php

namespace App\Service;

use App\Repository\ArticlesRepository;
use App\Repository\InterviewsRepository;
use App\Repository\ReportsRepository;
use App\Repository\ReviewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ArticlesService
{    
    private $articlesRepository;
    private $reviewsRepository;
    private $reportsRepository;
    private $interviewsRepository;
    private $detailService;
    private $serializer;
    private $entityManager;

    public function __construct(
        ArticlesRepository $articlesRepository,
        ReviewsRepository $reviewsRepository,
        ReportsRepository $reportsRepository,
        InterviewsRepository $interviewsRepository,
        DetailService $detailService,
        NormalizerInterface $serializer,
        EntityManagerInterface $entityManager,
    ) {
        $this->articlesRepository = $articlesRepository;
        $this->reviewsRepository = $reviewsRepository;
        $this->reportsRepository = $reportsRepository;
        $this->interviewsRepository = $interviewsRepository;
        $this->detailService = $detailService;
        $this->serializer = $serializer;
        $this->entityManager = $entityManager;
    }

    public function getDetail(string $slug, string $type = 'clanky'): array
    {
        $url = $type . '/' . $slug;

        switch ($type) {
            case 'recenze':
                $detailData = $this->reviewsRepository->findReviewDetailData($url);
                break;
            
            case 'clanky':
                $detailData = $this->articlesRepository->findDetailData($url);
                break;
        }

        return $this->detailService->parseDetailData($detailData);
    }

    public function getFullBoardData(int $boxCount): array
    {
        $articles = $this->articlesRepository->findAll();
        $interviews = $this->interviewsRepository->findAll();
        $reviews = $this->reviewsRepository->findAll();
        $reports = $this->reportsRepository->findAll();

        $combinedResults = array_merge($articles, $interviews, $reviews, $reports);

        usort($combinedResults, function ($a, $b) {
            return $b->getPublishDate() <=> $a->getPublishDate();
        });

        $slicedResult = array_slice($combinedResults, $boxCount, 9);

        return $this->serializeEntities($slicedResult);
    }    

    public function getArticlesData(string $id): array
    {
        switch ($id) {
            case '1':
                return $this->articlesRepository->findAllArticles();
                break;
                
            case '2':
                return $this->reviewsRepository->findReviews();
                break;

            case '3':
                return $this->reportsRepository->findReports();
                break;
                
            case '4':
                return $this->interviewsRepository->findInterviews();
                break;
            
            case '5':
                return $this->CategoryToType($this->articlesRepository->findAlbums());
                break;
                
            case '6':                       
                return $this->CategoryToType($this->articlesRepository->findOthers());
                break;
                
            default:
                return $this->articlesRepository->findAllArticles();
                break;
        }        
    }

    public function getBoardData(string $activeCategory, int $boxCount): array
    {
        switch ($activeCategory) {
            case 'all':
                return $this->getFullBoard($boxCount);
                break;
                
            case 'reviews':
                return $this->reviewsRepository->findReviewsBoard($boxCount);
                break;

            case 'reports':
                return $this->reportsRepository->findReportsBoard($boxCount);
                break;
                
            case 'interviews':
                return $this->interviewsRepository->findInterviewsBoard($boxCount);
                break;
            
            case 'albums':
                return $this->CategoryToType($this->articlesRepository->findAlbumsBoard($boxCount));
                break;
                
            case 'others':                       
                return $this->CategoryToType($this->articlesRepository->findOthersBoard($boxCount));
                break;
        }        
    }

    private function CategoryToType(array $array): array
    {
        $array = array_map(function ($article) {
            if (isset($article['category'])) {
                $article['type'] = $article['category'];
                unset($article['category']);
            }
            return $article;
        }, $array);

        return $array;
    }

    private function getFullBoard(int $boxCount): array
    {
        $conn = $this->entityManager->getConnection();

        $sql = "SELECT id, author_name, comments_count, publish_date, title, category, image_reference, url_path FROM articles
                UNION
                SELECT id, author_name, comments_count, publish_date, title, type, image_reference, url_path FROM interviews
                UNION
                SELECT id, author_name, comments_count, publish_date, title, type, image_reference, url_path FROM reports
                WHERE publish_date < NOW() ORDER BY publish_date DESC LIMIT :boxCount, 9";

        $stmt = $conn->prepare($sql);
        $stmt->bindValue('boxCount', $boxCount, \PDO::PARAM_INT);

        $result = $stmt->executeQuery();
        $results = $result->fetchAllAssociative();

        foreach ($results as $key => $item) {
            if (isset($item['category'])) {
                $results[$key]['type'] = $item['category'];
                unset($results[$key]['category']);
            }
        }

        return $results;
    }

    private function serializeEntities(array $entities): array
    {
        return array_map(function ($entity) {
            return $this->serializer->normalize($entity);
        }, $entities);
    }
}