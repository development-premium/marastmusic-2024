<?php

namespace App\DTO;

class CommentsDTO {
    public $name;
    public $text;
    public $contentObjectId;
    public $url;
    public $title;
    public $type;
    public $urlPath;
    public $detailTitle;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    public function getContentObjectId()
    {
        return $this->contentObjectId;
    }

    public function setContentObjectId($contentObjectId)
    {
        $this->contentObjectId = $contentObjectId;

        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getUrlPath()
    {
        return $this->urlPath;
    }

    public function setUrlPath($urlPath)
    {
        $this->urlPath = $urlPath;

        return $this;
    }

    public function getDetailTitle()
    {
        return $this->detailTitle;
    }

    public function setDetailTitle($detailTitle)
    {
        $this->detailTitle = $detailTitle;

        return $this;
    }
}