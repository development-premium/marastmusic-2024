<?php 

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\Articles;

class ArticlesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $article = new Articles();
            $article->setContentObjectId($faker->randomNumber());
            $article->setCreatorId($faker->randomNumber());
            $article->setUrlPath($faker->url);
            $article->setTitle($faker->sentence);
            $article->setAuthorName($faker->name);
            $article->setAuthorEmail($faker->email);
            $article->setCategory($faker->word);
            $article->setImage($faker->imageUrl());
            $article->setIntro($faker->paragraph);
            $article->setBody($faker->text);
            $article->setPublishDate($faker->date());
            $article->setType($faker->word);
            $article->setCommentsCount($faker->randomNumber());
            $article->setTags($faker->words(3, true));
            $article->setImageReference($faker->imageUrl());
            $article->setRelatedContent($faker->words(3, true));
            $article->setViews($faker->randomNumber());
            $article->setImgMain($faker->imageUrl());
            $article->setPublic($faker->boolean);
            $article->setSlider($faker->text);
            $article->setIsRedesignVersion($faker->randomNumber());
            $article->setAdded($faker->date());
            $manager->persist($article);
        }

        $manager->flush();
    }
}
