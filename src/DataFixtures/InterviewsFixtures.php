<?php

namespace App\DataFixtures;

use App\Entity\Interviews;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class InterviewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $interview = new Interviews();
            $interview->setContentobjectId($faker->unique()->numberBetween(1, 1000));
            $interview->setCreatorId($faker->numberBetween(1, 10));
            $interview->setPublicationDate($faker->dateTime);
            $interview->setUrlPath($faker->slug);
            $interview->setTitle($faker->sentence);
            $interview->setAuthorName($faker->name);
            $interview->setAuthorEmail($faker->email);
            $interview->setBody($faker->text);
            $interview->setImage($faker->imageUrl);
            $interview->setPublishDate($faker->dateTime);
            $interview->setTags($faker->word);
            $interview->setType($faker->word);
            $interview->setCommentsCount($faker->randomDigit);
            $interview->setImageReference($faker->imageUrl);
            $interview->setRelatedContent($faker->text);
            $interview->setViews($faker->randomDigit);
            $interview->setIsRedesignVersion($faker->randomElement([0, 1]));
            $interview->setIntro($faker->paragraph);
            $interview->setImgMain($faker->imageUrl);
            $interview->setSlider($faker->text);
            $interview->setPublic($faker->randomElement([0, 1]));
            $interview->setAdded($faker->word);

            $manager->persist($interview);
        }

        $manager->flush();
    }
}
