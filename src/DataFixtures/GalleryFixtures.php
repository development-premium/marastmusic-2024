<?php

namespace App\DataFixtures;

use App\Entity\Gallery;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class GalleryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $gallery = new Gallery();
            $gallery->setContentobject_id($faker->randomNumber());
            $gallery->setCreator_id($faker->randomNumber());
            $gallery->setUrlPath($faker->url);
            $gallery->setTitle($faker->sentence);
            $gallery->setAuthor_name($faker->name);
            $gallery->setDescription($faker->text);
            $gallery->setPublish_date($faker->date($format = 'Y-m-d', $max = 'now'));
            $gallery->setImage($faker->imageUrl());
            $gallery->setEvent_date($faker->date($format = 'Y-m-d', $max = 'now'));
            $gallery->setVenue($faker->company);
            $gallery->setType($faker->word);
            $gallery->setRelated_bands($faker->text);
            $gallery->setTags($faker->text);
            $gallery->setImage_reference($faker->text);
            $gallery->setRelated_content($faker->text);
            $gallery->setComments_count($faker->randomNumber());
            $gallery->setIs_redesign_version($faker->randomElement($array = array (0, 1)));
            $gallery->setAdded($faker->date($format = 'Y-m-d', $max = 'now'));

            $manager->persist($gallery);
        }

        $manager->flush();
    }
}
