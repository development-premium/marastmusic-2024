<?php

namespace App\DataFixtures;

use App\Entity\Intro;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class IntroFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 3; $i++) {
            $banner = new Intro();
            $banner->setUrlPath($faker->url)
                ->setTitle($faker->sentence)
                ->setAuthorName($faker->name)
                ->setImage($faker->imageUrl)
                ->setPublishDate($faker->dateTimeThisYear)
                ->setType($faker->word)
                ->setCommentsCount($faker->randomNumber)
                ->setTags(implode(',', $faker->words));

            $manager->persist($banner);
        }

        $manager->flush();
    }
}