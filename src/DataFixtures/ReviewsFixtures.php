<?php

namespace App\DataFixtures;

use App\Entity\Reviews;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReviewsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $review = new Reviews();
            $review->setContentobjectId($faker->randomNumber());
            $review->setPublicationDate($faker->dateTime);
            $review->setUrlPath($faker->url);
            $review->setTitle($faker->sentence);
            $review->setAuthorName($faker->name);
            $review->setAuthorEmail($faker->email);
            $review->setBody($faker->text);
            $review->setImage($faker->imageUrl);
            $review->setPublishDate($faker->dateTime);
            $review->setTags($faker->word);
            $review->setType($faker->word);
            $review->setCommentsCount($faker->randomDigit);
            $review->setImageReference($faker->word);
            $review->setRelatedContent($faker->word);
            $review->setViews($faker->randomDigit);
            $review->setIsRedesignVersion($faker->randomDigit);
            $review->setIntro($faker->sentence);
            $review->setImgMain($faker->imageUrl);
            $review->setSlider($faker->sentence);
            $review->setPublic($faker->randomDigit);
            $review->setAdded($faker->word);
            $review->setRating($faker->word);
            $review->setBand($faker->word);
            $review->setAlbum($faker->word);
            $review->setAlbumid($faker->word);

            $manager->persist($review);
        }

        $manager->flush();
    }
}
