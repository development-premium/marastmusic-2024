<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Comments;

class CommentsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 20; $i++) {
            $comment = new Comments();
            $comment->setLanguageId(1);
            $comment->setCreated(time());
            $comment->setModified(time());
            $comment->setUserId($i + 1);
            $comment->setSessionKey('session_key' . $i);
            $comment->setIp('127.0.0.1');
            $comment->setContentobjectId((string)($i + 1));
            $comment->setParentCommentId(0);
            $comment->setName('User' . $i);
            $comment->setEmail('user' . $i . '@example.com');
            $comment->setUrl('http://example.com');
            $comment->setText('This is a comment text ' . $i);
            $comment->setStatus(1);
            $comment->setTitle('Title' . $i);
            $comment->setType('Type' . $i);
            $comment->setTargetId($i + 1);
            $comment->setIsOld(0);
            $comment->setDetailTitle('Detail Title' . $i);
            $comment->setUrlPath('/path' . $i);
            
            $manager->persist($comment);
        }
        
        $manager->flush();
    }
}
