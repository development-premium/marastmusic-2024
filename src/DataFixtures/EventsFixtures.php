<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Events;
use Faker\Factory;

class EventsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $event = new Events();

            $event->setPublication_date($faker->unixTime());
            $event->setUrl_path($faker->slug);
            $event->setTitle($faker->sentence(6, true));
            $event->setBands(implode(',', $faker->words(3)));
            $event->setImage($faker->imageUrl());
            $event->setVenue($faker->company);
            $event->setDate($faker->date());
            $event->setTime($faker->time());
            $event->setTags(implode(',', $faker->words(5)));
            $event->setText($faker->paragraphs(3, true));
            $event->setFacebook($faker->url);
            $event->setType($faker->word);
            $event->setComments_count($faker->randomNumber());
            $event->setImage_reference($faker->imageUrl());
            $event->setIs_redesign_version($faker->randomNumber(1));
            $event->setRelated_content($faker->sentence(6, true));
            $event->setImg_main($faker->imageUrl());
            $event->setPublic($faker->boolean);

            $manager->persist($event);
        }

        $manager->flush();
    }
}
