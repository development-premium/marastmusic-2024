<?php 

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class NewsFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $news = new News();
            $news->setContentobjectId($faker->randomDigit);
            $news->setCreatorId($faker->randomNumber());
            $news->setPublicationDate($faker->dateTimeThisYear);
            $news->setUrlPath($faker->slug);
            $news->setTitle($faker->sentence);
            $news->setAuthorName($faker->name);
            $news->setAuthorEmail($faker->email);
            $news->setBody($faker->paragraph);
            $news->setImage($faker->imageUrl());
            $news->setPublishDate($faker->dateTimeThisYear);
            $news->setTags($faker->words(3, true));
            $news->setRelatedBand($faker->randomNumber());
            $news->setRelatedAlbum($faker->randomNumber());
            $news->setRelatedNews($faker->word);
            $news->setType($faker->word);
            $news->setCommentsCount($faker->randomNumber());
            $news->setImageReference($faker->imageUrl());
            $news->setRelatedContent($faker->sentence);
            $news->setViews($faker->randomNumber());
            $news->setIsRedesignVersion($faker->randomElement([0, 1]));
            $news->setIntro($faker->paragraph);
            $news->setOptionalTag($faker->word);
            $news->setImgMain($faker->imageUrl());
            $news->setSlider($faker->paragraph);
            $news->setPublic($faker->randomElement([0, 1]));
            $news->setAdded($faker->dateTimeThisYear->format('Y-m-d H:i:s'));

            $manager->persist($news);
        }

        $manager->flush();
    }
}
