<?php

namespace App\DataFixtures;

use App\Entity\Reports;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ReportsFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $report = new Reports();
            $report->setContentobject_id($faker->unique()->numberBetween(1, 1000));
            $report->setCreator_id($faker->numberBetween(1, 10));
            $report->setUrl_path($faker->slug);
            $report->setTitle($faker->sentence);
            $report->setAuthor_name($faker->name);
            $report->setAuthor_email($faker->email);
            $report->setCategory($faker->word);
            $report->setImage($faker->imageUrl);
            $report->setEvent_date($faker->date);
            $report->setVenue($faker->word);
            $report->setIntro($faker->paragraph);
            $report->setBody($faker->text);
            $report->setPublish_date($faker->date);
            $report->setType($faker->word);
            $report->setComments_count($faker->randomDigit);
            $report->setTags($faker->word);
            $report->setImage_reference($faker->imageUrl);
            $report->setRelated_content($faker->text);
            $report->setViews($faker->randomDigit);
            $report->setIs_redesign_version($faker->randomElement([0, 1]));
            $report->setImg_main($faker->imageUrl);
            $report->setPublic($faker->randomElement([true, false]));
            $report->setSlider($faker->text);
            $report->setAdded($faker->word);

            $manager->persist($report);
        }

        $manager->flush();
    }
}
