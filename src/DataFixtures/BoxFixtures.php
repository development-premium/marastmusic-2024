<?php

namespace App\DataFixtures;

use App\Entity\Box;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BoxFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $box = new Box();
            $box->setTitle($faker->words(3, true));
            $box->setSubtitle($faker->sentence);
            $box->setImage($faker->imageUrl());
            $box->setLink($faker->url);
            $box->setDate($faker->dateTimeThisYear);

            $manager->persist($box);
        }

        $manager->flush();
    }
}
