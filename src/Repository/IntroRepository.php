<?php

namespace App\Repository;

use App\Entity\Intro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class IntroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Intro::class);
    }

    public function getDataByUrl(string $urlPath)
    {
        $urlPath = '%' . $urlPath . '%';
        $connection = $this->getEntityManager()->getConnection();

        $sql = "
            SELECT title, author_name, image_reference, publish_date, type, comments_count, tags 
            FROM articles WHERE url_path LIKE :urlPath
            UNION ALL
            SELECT title, author_name, image_reference, publish_date, type, comments_count, tags 
            FROM interviews WHERE url_path LIKE :urlPath
            UNION ALL
            SELECT title, author_name, image_reference, publish_date, type, comments_count, tags 
            FROM reports WHERE url_path LIKE :urlPath
            UNION ALL
            SELECT title, author_name, image_reference, publish_date, type, comments_count, tags 
            FROM reviews WHERE url_path LIKE :urlPath
            UNION ALL
            SELECT title, author_name, image_reference, publish_date, type, comments_count, tags 
            FROM news WHERE url_path LIKE :urlPath
            LIMIT 1
        ";

        $stmt = $connection->prepare($sql);
        return $stmt->executeQuery(['urlPath' => $urlPath])->fetchAllAssociative();
    }

    public function save($entity): void 
    {
        $entityManager = $this->getEntityManager();

        $entityManager->persist($entity);
        $entityManager->flush();
    }
}
