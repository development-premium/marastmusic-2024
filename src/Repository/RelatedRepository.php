<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class RelatedRepository extends ServiceEntityRepository
{
    private $reportsRepository;
    private $reviewsRepository;
    private $newsRepository;
    private $interviewsRepository;
    private $articlesRepository;

    public function __construct(
        ReportsRepository $reportsRepository,
        ReviewsRepository $reviewsRepository,
        NewsRepository $newsRepository,
        InterviewsRepository $interviewsRepository,
        ArticlesRepository $articlesRepository
    ) {
        $this->reportsRepository = $reportsRepository;
        $this->reviewsRepository = $reviewsRepository;
        $this->newsRepository = $newsRepository;
        $this->interviewsRepository = $interviewsRepository;
        $this->articlesRepository = $articlesRepository;
    }

    public function getRelated(string $relatedContent): array
    {
        if ($relatedContent === null || empty($relatedContent)) {
            return [];
        }

        $relatedContentArray = explode(",", $relatedContent);
        $result = [];

        foreach ($relatedContentArray as $relatedGroup) {
            [$type, $id] = explode(":", $relatedGroup) + [null, null];

            if ($type === null || $id === null) {
                return [];
            }
            
            switch ($type) {
                case 'report':
                    $result[] = $this->reportsRepository->find($id);
                    break;
                case 'recenze':
                    $result[] = $this->reviewsRepository->find($id);
                    break;
                case 'novinka':
                    $result[] = $this->newsRepository->find($id);
                    break;
                case 'interview':                    
                    $result[] = $this->interviewsRepository->find($id);
                    break;
                case 'rozhovor':
                    $result[] = $this->interviewsRepository->find($id);
                    break;
                case 'article':
                    $result[] = $this->articlesRepository->find($id);
                    break;
            }
        }

        return array_filter($result);
    }
}
