<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;

class ContentRepository
{
    private $entityManager;
    
    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    public function getRelatedContent(string $tags): string
    {
        $connection = $this->entityManager->getConnection();

        $tagsArray = explode(",", $tags);
        $relatedArray = [];

        foreach ($tagsArray as $tag) {
            if (count($relatedArray) >= 3) {
                break;
            }

            $tagLike = "%" . $tag . "%";
            $sql = "SELECT id, type, publish_date FROM articles WHERE tags LIKE ?
                    UNION ALL
                    SELECT id, type, publish_date FROM interviews WHERE tags LIKE ?
                    UNION ALL
                    SELECT id, type, publish_date FROM reports WHERE tags LIKE ?
                    UNION ALL
                    SELECT id, type, publish_date FROM reviews WHERE tags LIKE ?
                    UNION ALL
                    SELECT id, type, publish_date FROM news WHERE tags LIKE ?
                    ORDER BY publish_date DESC LIMIT 3";

            $stmt = $connection->prepare($sql);
            $stmt->bindValue(1, $tagLike);
            $stmt->bindValue(2, $tagLike);
            $stmt->bindValue(3, $tagLike);
            $stmt->bindValue(4, $tagLike);
            $stmt->bindValue(5, $tagLike);

            $res = $stmt->executeQuery()->fetchAllAssociative();

            foreach ($res as $relatedResultArray) {
                if (count($relatedArray) >= 3) {
                    break;
                }

                $relatedResultString = $relatedResultArray['type'] . ":" . $relatedResultArray['id'];
                if (!in_array($relatedResultString, $relatedArray)) {
                    $relatedArray[] = $relatedResultString;
                }
            }
        }

        if (count($relatedArray) >= 3) {
            return implode(",", $relatedArray);
        } else {
            $sql = "SELECT id FROM news ORDER BY publish_date DESC LIMIT 3";
            $res = $connection->executeQuery($sql)->fetchAllAssociative();
            $relatedArrayString = "novinka:" . $res[0]['id'] . ",novinka:" . $res[1]['id'] . ",novinka:" . $res[2]['id'];

            return $relatedArrayString;
        }
    }

    public function findLastEntries(): array
    {        
        $connection = $this->entityManager->getConnection();

        $query = "
            SELECT url_path, added FROM articles
            UNION ALL
            SELECT url_path, added FROM interviews
            UNION ALL
            SELECT url_path, added FROM reports
            UNION ALL
            SELECT url_path, added FROM reviews
            UNION ALL
            SELECT url_path, added FROM news
            ORDER BY added DESC LIMIT 10
        ";

        $stmt = $connection->prepare($query);
        return $stmt->executeQuery()->fetchAllAssociative();
    }
}
