<?php

namespace App\Repository;

use App\Entity\News;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class NewsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, News::class);
    }

    public function findLastNews(): array
    {
        return $this->findBy(['public' => 1], ['publish_date' => 'DESC'], 3);
    }

    public function getCmsListQueryBuilder($search = null)
    {
        $qb = $this->createQueryBuilder('q');

        $qb->select('q.title', 'q.id', 'q.author_name', 'q.added', 'q.publish_date')
        ->orderBy('q.added', 'DESC');

        if ($search) {
            $qb->andWhere('q.title LIKE :search OR q.author_name LIKE :search')
               ->setParameter('search', '%' . $search . '%');
        }

        return $qb;
    }

    public function findDetailData(string $urlPath): array
    {
        $qb = $this->createQueryBuilder('n');

        return $qb
            ->select('n.body', 'n.tags', 'n.related_content', 'n.type', 'n.author_name', 'n.intro', 'n.contentobject_id', 'n.title', 'n.img_main', 'n.publish_date', 'n.url_path', 'n.optional_tag', 'n.slider')
            ->where('n.url_path = :urlPath')
            ->setParameter('urlPath', $urlPath)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findBoardData(int $boxCount): array
    {
        $qb = $this->createQueryBuilder('n');

        $query = $qb
            ->select('n.id, n.author_name, n.comments_count, n.publish_date, n.title, n.image_reference, n.type, n.url_path, n.is_redesign_version')
            ->where('n.publish_date < :now')
            ->andWhere('n.public = :public')
            ->orderBy('n.publish_date', 'DESC')
            ->setParameter('now', new \DateTime())
            ->setParameter('public', 1)
            ->setMaxResults(9)
            ->setFirstResult($boxCount)
            ->getQuery();

        return $query->getArrayResult();
    }

    public function incrementCommentsCountForNews(string $url): int
    {
        $qb = $this->createQueryBuilder('r')
            ->update('App\Entity\News', 'r')
            ->set('r.comments_count', 'r.comments_count + 1')
            ->where('r.url_path LIKE :url')
            ->setParameter('url', '%' . $url . '%');

        return $qb->getQuery()->execute();
    }
}
