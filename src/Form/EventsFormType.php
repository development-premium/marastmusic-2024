<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class EventsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('event', TextType::class, [
                'attr' => ['placeholder' => 'Název akce'],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 100])
                ]
            ])
            ->add('date', TextType::class, [
                'attr' => ['placeholder' => 'Datum'],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Date(),
                ]
            ])
            ->add('club', TextType::class, [
                'attr' => ['placeholder' => 'Klub'],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 50])
                ]
            ])
            ->add('facebook', TextType::class, [
                'attr' => ['placeholder' => 'Facebook event'],
                'required' => false,
                'constraints' => [
                    new Assert\Url()
                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Odeslat'
            ]);
    }
}
