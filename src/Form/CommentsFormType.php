<?php

namespace App\Form;

use App\DTO\CommentsDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommentsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'attr' => ['class' => 'c-detail-form__input', 'placeholder' => 'Jméno']
            ])
            ->add('text', TextareaType::class, [
                'attr' => ['class' => 'c-detail-form__input', 'placeholder' => 'Zpráva', 'rows' => 1]
            ])
            ->add('contentobject_id', HiddenType::class)
            ->add('url', HiddenType::class)
            ->add('title', HiddenType::class)
            ->add('type', HiddenType::class)
            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => CommentsDTO::class,
        ]);
    }
}
