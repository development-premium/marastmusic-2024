<?php

namespace App\Controller;

use App\Form\CommentsFormType;
use App\Service\CommentsService;
use App\Service\DetailService;
use App\Service\GalleryService;
use App\Service\TopContentService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GalleryController extends BaseController
{
    private $galleryService;
    private $detailService;
    private $commentsService;
    private $topContentService;

    public function __construct(
        GalleryService $galleryService,
        DetailService $detailService,
        CommentsService $commentsService,
        TopContentService $topContentService,
    ) {
        $this->galleryService = $galleryService;
        $this->detailService = $detailService;
        $this->commentsService = $commentsService;
        $this->topContentService = $topContentService;
    }

    #[Route('/galerie/{slug}', name: 'app_gallery')]
    public function index(string $slug): Response
    {
        $images = $this->galleryService->getImages($slug);
        $galleryData = $this->galleryService->getGalleryDetail($slug);
        $relatedContent = $this->detailService->getRelatedContent($galleryData['related_content']);
        $detailComments = $this->commentsService->getDetail($galleryData['contentobject_id']);

        $commentsDTO = $this->commentsService->createDefaultCommentsDTO($galleryData, $slug);
        $commentsForm = $this->createForm(CommentsFormType::class, $commentsDTO);

        return $this->render('gallery/index.html.twig', [
            'images' => $images,
            'relatedContent' => $relatedContent,
            'commentsForm' => $commentsForm->createView(),
            'galleryData' => $galleryData,
            'comments' => $detailComments,
        ]);
    }

    #[Route('/galerie', name: 'app_gallery_overview')]
    public function overview(): Response
    {
        $topContent = $this->topContentService->getContent();
        $topContentWinner = $this->topContentService->getTopItem();

        return $this->render('overview/overview.html.twig', [
            'title' => 'Galerie',
            'type' => 'gallery-overview',
            'overview_template' => 'gallery-board',
            'topContent' => $topContent,
            'topContentWinner' => $topContentWinner,
        ]);
    }

    #[Route('/gallery-board/{boxCount}', name: 'app_gallery_board')]
    public function galleryBoard(int $boxCount = 0): Response
    {
        $boardData = $this->galleryService->getBoardData($boxCount);
        return $this->render('overview/board-v5.html.twig', [
            'boardData' => $boardData,
        ]);
    }
}
