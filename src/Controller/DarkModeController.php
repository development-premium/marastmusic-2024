<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class DarkModeController extends AbstractController
{
    #[Route('/darkmode', name: 'app_darkmode', methods: ['POST'])]
    public function index(Request $request, SessionInterface $session): JsonResponse
    {
        $darkmode = $request->request->get('darkmode');
        $session->set('darkmode', $darkmode);

        return $this->json([
            'success' => true,
            'darkmode' => $darkmode
        ]);
    }
}
