<?php

namespace App\Controller;

use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    private $searchService;

    public function __construct(SearchService $searchService) {
        $this->searchService = $searchService;
    }

    #[Route('/search-result', name: 'app_search_result')]
    public function index(Request $request): Response
    {       
        $parameters = $request->request->all();

        $searchString = $parameters['searchString'];
        $type = $parameters['type'];
    
        $searchResult = $this->searchService->getList($searchString, $type);

        return $this->render('search-result.html.twig', [
            'searchResult' => $searchResult,
        ]);
    }
}
