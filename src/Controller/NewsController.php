<?php

namespace App\Controller;

use App\Form\CommentsFormType;
use App\Service\CommentsService;
use App\Service\DetailService;
use App\Service\NewsService;
use App\Service\TopContentService;
use App\Service\ViewCounterService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsController extends BaseController
{
    private $newsService;
    private $detailService;
    private $commentsService;
    private $topContentService;
    private $viewCounterService;

    public function __construct(
        NewsService $newsService,
        DetailService $detailService,
        CommentsService $commentsService,
        TopContentService $topContentService,        
        ViewCounterService $viewCounterService,
    ) {
        $this->newsService = $newsService;
        $this->detailService = $detailService;
        $this->commentsService = $commentsService;
        $this->topContentService = $topContentService;        
        $this->viewCounterService = $viewCounterService;
    }

    #[Route('/novinky/{url}', name: 'app_novinky')]
    public function index(string $url): Response
    {
        $detail = $this->newsService->getDetail($url);
        
        $relatedContent = $this->detailService->getRelatedContent($detail['related_content']);
        $detailComments = $this->commentsService->getDetail($detail['contentobject_id']);

        $this->viewCounterService->incrementView('novinka', $url);

        $commentsDTO = $this->commentsService->createDefaultCommentsDTO($detail, $url);

        $commentsForm = $this->createForm(CommentsFormType::class, $commentsDTO);

        return $this->render('detail/detail.html.twig', [
            'title' => 'Novinky',
            'detail' => $detail,
            'relatedContent' => $relatedContent,
            'commentsForm' => $commentsForm->createView(),
            'comments' => $detailComments,
        ]);
    }

    #[Route('/novinky', name: 'app_news_overview')]
    public function news(): Response
    {
        $topContent = $this->topContentService->getContent();
        $topContentWinner = $this->topContentService->getTopItem();

        return $this->render('overview/overview.html.twig', [
            'title' => 'Novinky',
            'type' => 'overview',
            'overview_template' => 'news-board',
            'topContent' => $topContent,
            'topContentWinner' => $topContentWinner,
        ]);
    }

    #[Route('/news-board/{boxCount}', name: 'app_news_board')]
    public function newsBoard(int $boxCount = 0): Response
    {
        $boardData = $this->newsService->getBoardData($boxCount);
        return $this->render('overview/board.html.twig', [
            'boardData' =>$boardData,
        ]);
    }
}
