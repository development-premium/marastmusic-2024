<?php 

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    protected function render($view, array $parameters = [], Response $response = null, Request $request = null): Response
    {
        $requestStack = $this->container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        if ($request && $request->hasSession()) {
            $parameters['darkmode'] = $request->getSession()->get('darkmode', false);
        }

        return parent::render($view, $parameters, $response);
    }
}
