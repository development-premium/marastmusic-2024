<?php

namespace App\Controller;

use App\Service\TagsService;
use App\Service\TopContentService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TagsController extends AbstractController
{    
    private $topContentService;
    private $tagsService;

    public function __construct(
        TopContentService $topContentService,   
        TagsService $tagsService,
    ) {
        $this->topContentService = $topContentService;  
        $this->tagsService = $tagsService;
    }

    #[Route('/tagy/{tag}', name: 'app_tags')]
    public function index(string $tag): Response
    {
        $topContent = $this->topContentService->getContent();
        $topContentWinner = $this->topContentService->getTopItem();

        return $this->render('overview/overview.html.twig', [
            'title' => 'Novinky',
            'type' => 'tags-overview',
            'overview_template' => 'tags-board',
            'topContent' => $topContent,
            'topContentWinner' => $topContentWinner,
            'tag' => $tag,
        ]);
    }

    #[Route('/tags-board/{boxCount}', name: 'app_tags_board', defaults: ['boxCount' => 0])]
    public function newsBoard(Request $request, int $boxCount = 0): Response
    {
        $tag = $request->request->get('tag');

        $boardData = $this->tagsService->getBoardList($boxCount, $tag);
        return $this->render('overview/board.html.twig', [
            'boardData' =>$boardData,
        ]);
    }
}    