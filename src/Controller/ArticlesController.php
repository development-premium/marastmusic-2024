<?php

namespace App\Controller;

use App\Form\CommentsFormType;
use App\Service\ArticlesService;
use App\Service\CommentsService;
use App\Service\DetailService;
use App\Service\TopContentService;
use App\Service\ViewCounterService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticlesController extends BaseController
{
    private $articlesService;
    private $detailService;
    private $commentsService;
    private $topContentService;
    private $viewCounterService;

    public function __construct(
        ArticlesService $articlesService,
        DetailService $detailService,
        CommentsService $commentsService,
        TopContentService $topContentService,
        ViewCounterService $viewCounterService,
    ) {
        $this->articlesService = $articlesService;
        $this->detailService = $detailService;
        $this->commentsService = $commentsService;
        $this->topContentService = $topContentService;
        $this->viewCounterService = $viewCounterService;
    }

    #[Route('/clanky', name: 'app_articles_overview')]
    public function overview(Request $request): Response
    {
        $category = $request->query->get('kategorie');
        
        $topContent = $this->topContentService->getContent();
        $topContentWinner = $this->topContentService->getTopItem();

        return $this->render('overview/overview.html.twig', [
            'title' => 'Články',
            'category' => $category,
            'type' => 'articles',
            'overview_template' => 'articles-board',
            'overview_type' => 'articles-overview',
            'topContent' => $topContent,
            'topContentWinner' => $topContentWinner,
        ]);
    }

    #[Route('/clanky/{slug}', name: 'app_article_detail')]
    public function detail(string $slug): Response
    {
        return $this->renderDetailPage($slug, 'clanky');
    }

    #[Route('/recenze/{slug}', name: 'app_review_detail')]
    public function review(string $slug): Response
    {
        return $this->renderDetailPage($slug, 'recenze');
    }

    #[Route('/articles-board/{activeCategory}', name: 'app_articles_board')]
    public function board(Request $request, string $activeCategory): Response
    {
        $boxCount = $request->request->get('boxCount', 0);
        $boardData = $this->articlesService->getBoardData($activeCategory, $boxCount);

        return $this->render('overview/board.html.twig', [
            'title' => 'Články',
            'boardData' => $boardData,
        ]);
    }

    private function renderDetailPage(string $slug, string $type): Response
    {
        $detail = $this->articlesService->getDetail($slug, $type);

        $detailComments = $this->commentsService->getDetail($detail['contentobject_id']);
        
        $this->viewCounterService->incrementView($type, $slug);

        $relatedContent = $this->detailService->getRelatedContent($detail['related_content']);
        $commentsDTO = $this->commentsService->createDefaultCommentsDTO($detail, $slug);
        $commentsForm = $this->createForm(CommentsFormType::class, $commentsDTO);

        return $this->render('detail/detail.html.twig', [
            'title' => $type === 'clanky' ? 'Články' : 'Recenze',
            'detail' => $detail,
            'relatedContent' => $relatedContent,
            'commentsForm' => $commentsForm->createView(),
            'comments' => $detailComments,
        ]);
    }
}
