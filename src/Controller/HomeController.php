<?php

namespace App\Controller;

use App\Service\ArticlesService;
use App\Service\BoxService;
use App\Service\CommentsService;
use App\Service\EventsService;
use App\Service\GalleryService;
use App\Service\IntroService;
use App\Service\NewsService;
use App\Service\SpotifyService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends BaseController
{
    private $introService;
    private $newsService;
    private $boxService;
    private $eventsService;
    private $galleryService;

    public function __construct(
        IntroService $introService,
        NewsService $newsService,
        BoxService $boxService, 
        EventsService $eventsService,
        GalleryService $galleryService,
    ) {
        $this->introService = $introService;
        $this->newsService = $newsService;
        $this->boxService = $boxService;
        $this->eventsService = $eventsService;
        $this->galleryService = $galleryService;
    }

    #[Route('/', name: 'app_home')]
    public function index(): Response
    {
        $intro = $this->introService->getIntroData();
        $news = $this->newsService->getNewsData();
        $box = $this->boxService->getBoxdata();
        $events = $this->eventsService->getEventsData();
        $gallery = $this->galleryService->getGalleryData();
        
        $response = $this->render('home/index.html.twig', [
            'introBlock1' => $intro[0],
            'introBlock2' => $intro[1],
            'introBlock3' => $intro[2],
            'news' => $news,
            'box' => $box,
            'events' => $events,
            'gallery' => $gallery,
        ]);

        $response->headers->set('Cache-Control', 'no-cache, no-store, must-revalidate');
        $response->headers->set('Pragma', 'no-cache');
        $response->headers->set('Expires', '0');
        
        return $response;
    }

    #[Route('spotify-playlist/{id}', name: 'app_spotify')]
    public function spotify(SpotifyService $spotifyService, string $id): Response
    {
        $template = $spotifyService->getPlaylist($id);
        return $this->render($template);
    }

    #[Route('comments/{id}', name: 'app_comments')]
    public function comments(CommentsService $commentsService, string $id): Response
    {       
        $comments = $commentsService->getResult($id);
        return $this->render('home/comments/comments-result.html.twig', [
            'comments' => $comments,
        ]);
    }

    #[Route('articles-result/{id}', name: 'app_articles')]
    public function articles(ArticlesService $articlesService, string $id): Response
    {
        $articles = $articlesService->getArticlesData($id);
        
        return $this->render('home/articles/articles-result.html.twig', [
            'articles' => $articles,
        ]);
    }
}
