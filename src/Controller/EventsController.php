<?php

namespace App\Controller;

use App\Form\CommentsFormType;
use App\Form\EventsFormType;
use App\Service\CommentsService;
use App\Service\DetailService;
use App\Service\EventsService;
use App\Service\TopContentService;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class EventsController extends BaseController
{
    private $eventsService;
    private $detailService;
    private $commentsService;
    private $topContentService;
    private $kernel;

    public function __construct(
        EventsService $eventsService,
        DetailService $detailService,
        CommentsService $commentsService,
        TopContentService $topContentService,
        KernelInterface $kernel,
    ) {
        $this->eventsService = $eventsService;
        $this->detailService = $detailService;
        $this->commentsService = $commentsService;
        $this->topContentService = $topContentService;
        $this->kernel = $kernel;
    }

    #[Route('/pridat-akci', name: 'app_add_event')]
    public function addEvent(): Response
    {
        $eventForm = $this->createForm(EventsFormType::class);

        return $this->render('events/add-event.html.twig', [
            'eventForm' => $eventForm->createView()
        ]);
    }

    #[Route('/save-event-img', name: 'app_save_event_img')]
    public function saveEventImg(Request $request): Response
    {
        $base64Image = $request->request->get('image');

        if ($base64Image) {
            $imageData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $base64Image));
            $filename = uniqid() . '.png';
            $projectDir = $this->kernel->getProjectDir();
            $filePath = $projectDir . '/public/images/user-events/' . $filename;
            file_put_contents($filePath, $imageData);
        }    
        
        return new Response('images/user-events/' . $filename);
    }

    #[Route('/save-event-data', name: 'app_save_event_data')]
    public function saveEventData(Request $request, LoggerInterface $logger): JsonResponse
    {
        try {
            $this->eventsService->saveUserEvent($request);

            return new JsonResponse([
                'status' => 'success',
                'message' => 'User event saved successfully.'
            ]);
        } catch (\Exception $e) {

            $logger->error('Error saving user event: ' . $e->getMessage());

            return new JsonResponse([
                'status' => 'error',
                'message' => 'An error occurred while saving event.'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }
    }

    #[Route('/akce/{url}', name: 'app_events_detail')]
    #[Route('/tiskove_zpravy/{url}', name: 'app_press_detail')]
    public function index(string $url, Request $request): Response
    {
        $currentRoute = $request->attributes->get('_route');
        
        if ($currentRoute === 'app_events_detail') {
            $type = 'akce';
        } else if ($currentRoute === 'app_press_detail') {
            $type = 'tiskove_zpravy';
        }

        $detail = $this->eventsService->getDetail($url, $type);

        if ($detail['multiday_string'] !== "") {
            $date = $detail['multiday_string'];
        } else {
            $date = date("d.m", strtotime($detail['date']));
        }
        
        $relatedContent = $this->detailService->getRelatedContent($detail['related_content']);

        $detailComments = $this->commentsService->getDetail($detail['contentobject_id']);

        $commentsDTO = $this->commentsService->createDefaultCommentsDTO($detail, $url);

        $commentsForm = $this->createForm(CommentsFormType::class, $commentsDTO);

        return $this->render('detail/detail.html.twig', [
            'title' => 'Akce',
            'detail' => $detail,
            'date' => $date,
            'relatedContent' => $relatedContent,
            'commentsForm' => $commentsForm->createView(),
            'comments' => $detailComments,
        ]);
    }

    #[Route('/akce', name: 'app_events_overview')]
    public function overview(): Response
    {
        $topContent = $this->topContentService->getContent();
        $topContentWinner = $this->topContentService->getTopItem();
        
        return $this->render('overview/overview.html.twig', [
            'title' => 'Akce',
            'type' => 'events-overview',
            'overview_template' => 'events-board',
            'topContent' => $topContent,
            'topContentWinner' => $topContentWinner,
        ]);
    }

    #[Route('/events-board/{boxCount}', name: 'app_events_board')]
    public function eventsBoard(int $boxCount = 0): Response
    {
        $boardData = $this->eventsService->getBoardData($boxCount);
        return $this->render('overview/board-v2.html.twig', [
            'boardData' =>$boardData,
        ]);
    }    
}
