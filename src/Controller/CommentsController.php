<?php

namespace App\Controller;

use App\Exception\InvalidCsrfTokenException;
use App\Exception\ValidationFailedException;
use App\Service\CommentsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController extends AbstractController
{
    private $commentsService;

    public function __construct(CommentsService $commentsService) {
        $this->commentsService = $commentsService;
    }

    #[Route('/comments-form', name: 'app_comments_form')]
    public function index(Request $request): Response
    {
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException('Page not found');
        }
        
        try {
            $rawData = $request->getContent();

            parse_str($rawData, $formData);

            $referer = $request->headers->get('referer');
            $refererParts = parse_url($referer);
            $formData['urlPath'] = isset($refererParts['path']) ? ltrim($refererParts['path'], '/') : '';
    
            $this->commentsService->handleCommentsData($formData);
    
            return new Response('Comment processed successfully');
        } catch (InvalidCsrfTokenException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (ValidationFailedException $e) {
            return new Response($e->getMessage(), Response::HTTP_BAD_REQUEST);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    #[Route('/comments-detail', name: 'app_comments_detail')]
    public function detail(Request $request): Response
    {
        parse_str($request->getContent(), $output);
        $contentObjectId = $output['contentObjectId'];
        $detailComments = $this->commentsService->getDetail($contentObjectId);

        return $this->render('detail/detail-comments.html.twig', [
            'comments' => $detailComments,
        ]);
    }
}
