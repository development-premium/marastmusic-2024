<?php

namespace App\Entity;

use App\Repository\ArticlesRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ArticlesRepository::class)]
class Articles
{
    #[ORM\Id, ORM\GeneratedValue, ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'bigint')]
    private int $contentobject_id;

    #[ORM\Column(type: 'integer', nullable: true)]
    private int $creator_id;

    #[ORM\Column(type: 'string', length: 255)]
    private string $url_path;

    #[ORM\Column(type: 'string', length: 255)]
    private string $title;

    #[ORM\Column(type: 'string', length: 255)]
    private string $author_name;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private string $author_email;

    #[ORM\Column(type: 'string', length: 255)]
    private string $category;

    #[ORM\Column(type: 'string', length: 1000)]
    private string $image;

    #[ORM\Column(type: 'text', nullable: true)]
    private string $intro;

    #[ORM\Column(type: 'text')]
    private string $body;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $publish_date = null;

    #[ORM\Column(type: 'string', length: 255)]
    private string $type;

    #[ORM\Column(type: 'integer')]
    private int $comments_count = 0;

    #[ORM\Column(type: 'string', length: 2000)]
    private string $tags;

    #[ORM\Column(type: 'string', length: 1000)]
    private string $image_reference;

    #[ORM\Column(type: 'string', length: 255)]
    private string $related_content;

    #[ORM\Column(type: 'integer')]
    private int $views = 0;

    #[ORM\Column(type: 'string', length: 255)]
    private string $img_main;

    #[ORM\Column(type: 'boolean')]
    private bool $public;

    #[ORM\Column(type: 'text', nullable: true)]
    private string $slider;

    #[ORM\Column(type: 'integer')]
    private int $is_redesign_version;

    #[ORM\Column(type: 'string', length: 255)]
    private string $added;

    public function incrementViews()
    {
        $this->views++;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getContentObjectId(): int
    {
        return $this->contentobject_id;
    }

    public function getCreatorId(): int
    {
        return $this->creator_id;
    }

    public function getUrlPath(): string
    {
        return $this->url_path;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAuthorName(): string
    {
        return $this->author_name;
    }

    public function getAuthorEmail(): string
    {
        return $this->author_email;
    }

    public function getCategory(): string
    {
        return $this->category;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function getIntro(): string
    {
        return $this->intro;
    }

    public function getBody(): string
    {
        return $this->body;
    }

    public function getPublishDate(): ?\DateTimeInterface
    {
        return $this->publish_date;
    }

    public function setPublishDate(\DateTimeInterface $publish_date): static
    {
        $this->publish_date = $publish_date;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getCommentsCount(): int
    {
        return $this->comments_count;
    }

    public function getTags(): string
    {
        return $this->tags;
    }

    public function getImageReference(): string
    {
        return $this->image_reference;
    }

    public function getRelatedContent(): string
    {
        return $this->related_content;
    }

    public function getViews(): int
    {
        return $this->views;
    }

    public function getImgMain(): string
    {
        return $this->img_main;
    }

    public function getPublic(): bool
    {
        return $this->public;
    }

    public function getSlider(): string
    {
        return $this->slider;
    }

    public function getIsRedesignVersion(): int
    {
        return $this->is_redesign_version;
    }

    public function getAdded(): string
    {
        return $this->added;
    }

    public function setContentObjectId(int $contentobject_id): self
    {
        $this->contentobject_id = $contentobject_id;
        return $this;
    }

    public function setCreatorId(int $creator_id): self
    {
        $this->creator_id = $creator_id;
        return $this;
    }

    public function setUrlPath(string $url_path): self
    {
        $this->url_path = $url_path;
        return $this;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    public function setAuthorName(string $author_name): self
    {
        $this->author_name = $author_name;
        return $this;
    }

    public function setAuthorEmail(string $author_email): self
    {
        $this->author_email = $author_email;
        return $this;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;
        return $this;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;
        return $this;
    }

    public function setIntro(string $intro): self
    {
        $this->intro = $intro;
        return $this;
    }

    public function setBody(string $body): self
    {
        $this->body = $body;
        return $this;
    }

    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    public function setCommentsCount(int $comments_count): self
    {
        $this->comments_count = $comments_count;
        return $this;
    }

    public function setTags(string $tags): self
    {
        $this->tags = $tags;
        return $this;
    }

    public function setImageReference(string $image_reference): self
    {
        $this->image_reference = $image_reference;
        return $this;
    }

    public function setRelatedContent(string $related_content): self
    {
        $this->related_content = $related_content;
        return $this;
    }

    public function setViews(int $views): self
    {
        $this->views = $views;
        return $this;
    }

    public function setImgMain(string $img_main): self
    {
        $this->img_main = $img_main;
        return $this;
    }

    public function setPublic(bool $public): self
    {
        $this->public = $public;
        return $this;
    }

    public function setSlider(string $slider): self
    {
        $this->slider = $slider;
        return $this;
    }

    public function setIsRedesignVersion(int $is_redesign_version): self
    {
        $this->is_redesign_version = $is_redesign_version;
        return $this;
    }

    public function setAdded(string $added): self
    {
        $this->added = $added;
        return $this;
    }


}
