<?php

namespace App\Entity;

use App\Entity\Images;
use App\Repository\GalleryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: GalleryRepository::class)]
class Gallery
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;
    
    #[ORM\Column(type: "bigint")]
    private int $contentobjectId;

    #[ORM\Column(type: "integer")]
    private int $creator_id;

    #[ORM\Column(type: "string", length: 255)]
    private string $url_path;

    #[ORM\Column(type: "string", length: 255)]
    private string $title;

    #[ORM\Column(type: "string", length: 255)]
    private string $author_name;

    #[ORM\Column(type: "text")]
    private string $description;

    #[ORM\Column(type: "string", length: 255)]
    private string $publish_date;

    #[ORM\Column(type: "string", length: 1000)]
    private string $image;

    #[ORM\Column(type: "string", length: 255)]
    private string $event_date;

    #[ORM\Column(type: "string", length: 255)]
    private string $venue;

    #[ORM\Column(type: "string", length: 255)]
    private string $type;

    #[ORM\Column(type: "text")]
    private string $related_bands;

    #[ORM\Column(type: "text")]
    private string $tags;

    #[ORM\Column(type: "text")]
    private string $image_reference;

    #[ORM\Column(type: "string", length: 255)]
    private string $related_content;

    #[ORM\Column(type: "integer")]
    private int $comments_count = 0;

    #[ORM\Column(type: "integer")]
    private int $is_redesign_version = 1;

    #[ORM\Column(type: "string", length: 255)]
    private string $added;

    private string $photosCount;

    #[ORM\OneToMany(mappedBy: 'gallery', targetEntity: Images::class)]
    private Collection $images;

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getContentobjectId()
    {
        return $this->contentobjectId;
    }
    
    public function setContentobjectId($contentobjectId)
    {
        $this->contentobjectId = $contentobjectId;
        return $this;
    }
    
    public function getCreator_id()
    {
        return $this->creator_id;
    }
    
    public function setCreator_id($creator_id)
    {
        $this->creator_id = $creator_id;
        return $this;
    }
    
    public function getUrlPath()
    {
        return $this->url_path;
    }
    
    public function setUrlPath($url_path)
    {
        $this->url_path = $url_path;
        return $this;
    }
    
    public function getTitle()
    {
        return $this->title;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }
    
    public function getAuthorName()
    {
        return $this->author_name;
    }
    
    public function setAuthorName($author_name)
    {
        $this->author_name = $author_name;
        return $this;
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function getPublishDate()
    {
        return $this->publish_date;
    }
    
    public function setPublishDate($publish_date)
    {
        $this->publish_date = $publish_date;
        return $this;
    }
    
    public function getImage()
    {
        return $this->image;
    }
    
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
    
    public function getEventDate()
    {
        return $this->event_date;
    }
    
    public function setEventDate($event_date)
    {
        $this->event_date = $event_date;
        return $this;
    }
    
    public function getVenue()
    {
        return $this->venue;
    }
    
    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }
    
    public function getType()
    {
        return $this->type;
    }
    
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
    
    public function getRelated_bands()
    {
        return $this->related_bands;
    }
    
    public function setRelated_bands($related_bands)
    {
        $this->related_bands = $related_bands;
        return $this;
    }
    
    public function getTags()
    {
        return $this->tags;
    }
    
    public function setTags($tags)
    {
        $this->tags = $tags;
        return $this;
    }
    
    public function getImageReference()
    {
        return $this->image_reference;
    }
    
    public function setImageReference($image_reference)
    {
        $this->image_reference = $image_reference;
        return $this;
    }
    
    public function getRelatedContent()
    {
        return $this->related_content;
    }
    
    public function setRelatedContent($related_content)
    {
        $this->related_content = $related_content;
        return $this;
    }
    
    public function getComments_count()
    {
        return $this->comments_count;
    }
    
    public function setComments_count($comments_count)
    {
        $this->comments_count = $comments_count;
        return $this;
    }
    
    public function getIs_redesign_version()
    {
        return $this->is_redesign_version;
    }
    
    public function setIs_redesign_version($is_redesign_version)
    {
        $this->is_redesign_version = $is_redesign_version;
        return $this;
    }
    
    public function getAdded()
    {
        return $this->added;
    }
    
    public function setAdded($added)
    {
        $this->added = $added;
        return $this;
    }   

    public function getPhotosCount()
    {
        return $this->photosCount;
    }

    public function setPhotosCount($photosCount)
    {
        $this->photosCount = $photosCount;

        return $this;
    }

    /**
     * @return Collection<int, Images>
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): static
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
            $image->setGallery($this);
        }

        return $this;
    }

    public function removeImage(Images $image): static
    {
        if ($this->images->removeElement($image)) {
            if ($image->getGallery() === $this) {
                $image->setGallery(null);
            }
        }

        return $this;
    }
}
